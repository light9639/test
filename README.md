## :zap: Git을 사용한 연습페이지.

<img src="https://velog.velcdn.com/images/co_der/post/a33d181a-bec1-4a23-a775-0c309fd546bc/image.png" alt="Git">

- `git add`, `commit`, `vscode GUI`를 이용하여 파일을 저장하였음.<br>
- `git diff`, `difftool`, `VsCode Graph`를 사용하여 `commit`을 비교하였음.<br>
- `git merge`를 이용하여 `branch`를 병합하였고, `rebase`, `squash`를 사용하였음.<br>
- `git revert`, `reset`을 사용하여 이전파일로 돌아가는 연습을 하였음.<br>
- `git push`, `pull`을 사용하여 서버에 파일을 저장하고 다운 받았음.<br>
- `git stash`를 이용하여 일부 파일의 임시 보관 및 활용 연습을 해보았음.<br>
- `npm i -g gitmoji-cli`로 `gitmoji`를 설치한 후 `gitmoji -c`를 사용하여 `git commit`을 하였음.<br>
